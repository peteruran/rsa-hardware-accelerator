library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity binary is
	generic (
		C_block_size : integer := 256
	);
	port (
		--input controll
		valid_in : in std_logic;
		ready_in : out std_logic;

		--input data
		message : in std_logic_vector (C_block_size - 1 downto 0);
		key : in std_logic_vector (C_block_size - 1 downto 0);

		--output controll
		ready_out : in std_logic;
		valid_out : out std_logic;

		--output data
		result : out std_logic_vector(C_block_size - 1 downto 0);

		--modulus
		modulus : in std_logic_vector(C_block_size - 1 downto 0);

		--utility
		clk : in std_logic;
		reset_n : in std_logic

	);
end binary;
architecture binaryBehave of binary is

	-- Internal Blakley data signals
	signal m : unsigned (C_block_size - 1 downto 0);
	signal e : unsigned (C_block_size - 1 downto 0);
	signal n : unsigned (C_block_size - 1 downto 0);
	signal c : unsigned (C_block_size - 1 downto 0);

	signal binary_counter : unsigned (C_block_size - 1 downto 0);

	-- States for state machine
	type states_type is (binary_ready, first_blakley, second_blakley, shift_e, binary_done);
	signal binary_state : states_type;

	-- Binary module FIFO helpers
	signal valid_out_s : std_logic;
	signal ready_in_s : std_logic;

	-- Signals for interfacing with Blakley
	signal blakley_a_in : std_logic_vector (C_block_size - 1 downto 0);
	signal blakley_b_in : std_logic_vector (C_block_size - 1 downto 0);
	signal blakley_n_in : std_logic_vector (C_block_size - 1 downto 0);
	signal blakley_result : std_logic_vector (C_block_size - 1 downto 0);

	-- Blakley FIFO
	signal blakley_valid_in : std_logic;
	signal blakley_ready_in : std_logic;
	signal blakley_ready_out : std_logic;
	signal blakley_valid_out : std_logic;

begin
	-- Initialize Blakley
	i_blakley : entity work.blakley
		port map(
			clk             => clk,
			reset_n         => reset_n,

			a_in            => blakley_a_in,
			b_in            => blakley_b_in,
			n_in            => blakley_n_in,

			data_in_valid   => blakley_valid_in,
			data_in_ready   => blakley_ready_in,
			data_out_ready  => blakley_ready_out,
			data_out_valid  => blakley_valid_out,

			r_out           => blakley_result
		);
    -- FIFO helpers
    valid_out <= valid_out_s;
    ready_in <= ready_in_s;
    process (clk, reset_n)

    -- States for state machine
    variable state : states_type;

	begin
		-- Keep track of state for debugging
		binary_state <= state;

		-- Reset all registers async
		if reset_n = '0' then
			m <= (others => '0');
			e <= (others => '0');
			n <= (others => '0');
			c <= (others => '0');

			binary_counter <= (others => '0');

			blakley_a_in <= (others => '0');
			blakley_b_in <= (others => '0');
			blakley_n_in <= (others => '0');

			blakley_valid_in <= '0';
			blakley_ready_out <= '0';

			ready_in_s <= '0';
			valid_out_s <= '0';

			result <= (others => '0');

			state := binary_ready;

        -- State machine for binary method
		elsif rising_edge(clk) then

			-- Prepare circuit and load data
			if state = binary_ready then

				-- Input FIFO
				if valid_in = '1' and ready_in_s = '1' then

					m <= unsigned(message);
					e <= unsigned(key);
					n <= unsigned(modulus);

					ready_in_s <= '0';
					result <= (others => 'Z'); -- stop module from occupying data bus

					-- Determine initial value of c
					if key(255) = '1' then
						c <= unsigned(message);
					else
						c <= to_unsigned(1, c'length);
					end if;

					-- Enable binary algorithm
					state := first_blakley;

                -- Make circuit ready to take in data when in ready state
				else
					ready_in_s <= '1';

				end if;
				-- Runs the first round of Blakley algorithm
			elsif state = first_blakley then

				-- Return result when Blakley is done
				if blakley_ready_out = '1' and blakley_valid_out = '1' then
					c <= unsigned(blakley_result);
					blakley_ready_out <= '0';

					-- Determine whether second round of Blakley should be run
					if e(254) = '1' then
						state := second_blakley;
					else
						state := shift_e;
					end if;

                -- Data transfer done. Wait for result
				elsif blakley_ready_in = '1' and blakley_valid_in = '1' then
					blakley_valid_in <= '0';
					blakley_ready_out <= '1';
                -- Prepare input data for Blakley
				elsif blakley_valid_in = '0' and blakley_ready_out = '0' then
					blakley_a_in <= std_logic_vector(c);
					blakley_b_in <= std_logic_vector(c);
					blakley_n_in <= std_logic_vector(n);

					blakley_valid_in <= '1';

				end if;

            -- Runs the optional round of the Blakley algorithm
			elsif state = second_blakley then

				-- Return result when Blakley is done
				if blakley_ready_out = '1' and blakley_valid_out = '1' then
					c <= unsigned(blakley_result);
					blakley_ready_out <= '0';

					-- Go back to state shift_e
					state := shift_e;

					-- Data transfer done. Wait for result
				elsif blakley_ready_in = '1' and blakley_valid_in = '1' then
					blakley_valid_in <= '0';
					blakley_ready_out <= '1';

                -- Prepare input data for Blakley
				elsif blakley_valid_in = '0' and blakley_ready_out = '0' then
					blakley_a_in <= std_logic_vector(c);
					blakley_b_in <= std_logic_vector(m);
					blakley_n_in <= std_logic_vector(n);

					blakley_valid_in <= '1';

				end if;
				-- Shifts e and keeps track of progression with counter
			elsif state = shift_e then

				-- Disable Binary when done. Else, run Blakley again
				if (binary_counter = 254) then
					state := binary_done;

				else
					binary_counter <= binary_counter + 1;
					e <= shift_left(e, 1);
					state := first_blakley;
				end if;

			elsif state = binary_done then

				-- Binary FIFO out
				if ready_out = '1' and valid_out_s = '1' then

					-- Reset intermediate results
					c <= (others => '0');
					binary_counter <= (others => '0');

					result <= (others => 'Z'); -- stop module from occupying data bus
					valid_out_s <= '0';
					state := binary_ready;
                -- Write result to output port
				elsif ready_out = '1' then --NOTE: Ready before valid to avoid bus collision
					result <= std_logic_vector(c);
					valid_out_s <= '1';
				end if;

			end if;

		end if;

	end process;

end binaryBehave;
