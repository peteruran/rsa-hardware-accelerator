----------------------------------------------------------------------------------
-- Company: The Fashion Bruises
-- Engineer: Peter Uran
--
-- Create Date: 10/14/2019 06:24:37 PM
-- Design Name: Blakley module
-- Module Name: blakley - Behavioral
-- Project Name: Binary method test
-- Target Devices: Pynq
-- Tool Versions:
-- Description: Modular multiplication
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
entity blakley is
	generic (
		C_block_size : integer := 256
	);
	port (
		-- Input signals
		a_in : in std_logic_vector (C_block_size - 1 downto 0);
		b_in : in std_logic_vector (C_block_size - 1 downto 0);
		n_in : in std_logic_vector (C_block_size - 1 downto 0);
 
		clk : in std_logic;
		reset_n : in std_logic;
 
		-- Output signals
		r_out : out std_logic_vector (C_block_size - 1 downto 0);
 
		-- Input data FIFO ports
		data_in_valid : in std_logic;
		data_in_ready : out std_logic;
 
		-- Output data FIFO ports
		data_out_valid : out std_logic;
		data_out_ready : in std_logic
	);
 
end blakley;

architecture rtl of blakley is

	signal a : unsigned(C_block_size - 1 downto 0);
	signal b : unsigned(C_block_size - 1 downto 0);
	signal n : unsigned(C_block_size downto 0); -- IMPORTANT: n must have the extra bit for subtraction in r mod n
 
	signal blakley_counter : unsigned(C_block_size - 1 downto 0); --unnececarry large
 
	signal r : unsigned(C_block_size downto 0);
 
	signal data_out_valid_s : std_logic;
	signal data_in_ready_s : std_logic;
 
	-- State signal for debugging
	type states_type is (ready, enabled, done);
	signal blakley_state : states_type;
 

begin
	data_out_valid <= data_out_valid_s;
	data_in_ready <= data_in_ready_s;
 
	process (clk, reset_n)
 
	variable a_wide : unsigned(C_block_size downto 0);
	variable adder : unsigned(C_block_size downto 0);
	variable adder_minus_n : unsigned(C_block_size downto 0);
	variable adder_minus_2n : unsigned(C_block_size downto 0);
 
	-- Variable for state machine
	variable state : states_type;
 
	begin
		-- For debugging
		--blakley_state <= state;
 
		-- Reset all registers async
		if reset_n = '0' then
			a <= (others => '0');
			b <= (others => '0');
			n <= (others => '0');
 
			r <= (others => '0');
			r_out <= (others => '0');
			adder := (others => '0');
			adder_minus_n := (others => '0');
            adder_minus_2n := (others => '0');
 

			blakley_counter <= (others => '0');
 
			data_in_ready_s <= '0';
			data_out_valid_s <= '0';
 
			state := ready;
 
 
		elsif rising_edge(clk) then
 
            -- Set ready_in and load data from ports
			if state = ready then
 
				-- Input FIFO
				if data_in_valid = '1' and data_in_ready_s = '1' then
 
					a <= unsigned(a_in);
					b <= unsigned(b_in);
					n <= '0' & unsigned(n_in);
					data_in_ready_s <= '0';
 
					-- Enable
					state := enabled;
 
				else
					data_in_ready_s <= '1';
				end if;
 
            -- run Blakley algorithm
			elsif state = enabled then
 
 
				-- Disable Blakely when done
				blakley_counter <= blakley_counter + 1;
				if (blakley_counter = 255) then
					state := done;
				end if;
 
				a <= shift_left(a, 1); -- left shift register
 
				-- Add (a and b) with r_reg
				a_wide := (others => a(255));
				adder := (a_wide and '0' & b) + shift_left(r, 1);
				
				-- Intermediate results to speed up computation
				adder_minus_n := adder - n;
				adder_minus_2n := adder - shift_left(n, 1);
 
				-- r mod n
				if (adder < n) then
					r <= adder;
				elsif (adder_minus_n < n) then
					r <= adder_minus_n;
				elsif (adder_minus_2n < n) then
					r <= adder_minus_2n;
				end if;
 
			-- Write data to ports when Blakley is done
			elsif state = done then
 
				-- FIFO out
				if data_out_ready = '1' and data_out_valid_s = '1' then
 
					-- Reset intermediate results
					r <= (others => '0');
					a_wide := (others => '0');
					adder := (others => '0');
					adder_minus_n := (others => '0');
					adder_minus_2n := (others => '0');
					blakley_counter <= (others => '0');
 
					data_out_valid_s <= '0';
					state := ready;
				else
					r_out <= std_logic_vector(r(255 downto 0));
					data_out_valid_s <= '1';
				end if;
 
			end if;
		end if;
	end process;
end rtl;